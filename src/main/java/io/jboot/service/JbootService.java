package io.jboot.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import io.jboot.db.model.Columns;
import io.jboot.db.model.JbootModel;

public interface JbootService<M extends JbootModel<M>> {

	/**
	 * 根据ID查找model
	 *
	 * @param id
	 * @return
	 */
	M findById(Object id);

	/**
	 * 根据 Columns 查找单条数据
	 *
	 * @param columns
	 * @return
	 */
	M findFirstByColumns(Columns columns);

	/**
	 * 根据 Columns 查找单条数据
	 *
	 * @param columns
	 * @param orderBy
	 * @return
	 */
	M findFirstByColumns(Columns columns, String orderBy);

	/**
	 * 查找全部数据
	 *
	 * @return
	 */
	List<M> findAll();

	/**
	 * 根据 Columns 查找数据
	 *
	 * @param columns
	 * @return
	 */
	List<M> findListByColumns(Columns columns);

	/**
	 * 根据 Columns 查找数据
	 *
	 * @param columns
	 * @param orderBy
	 * @return
	 */
	List<M> findListByColumns(Columns columns, String orderBy);

	/**
	 * 根据 Columns 查找数据
	 *
	 * @param columns
	 * @param count
	 * @return
	 */
	List<M> findListByColumns(Columns columns, Integer count);

	/**
	 * 根据 Columns 查找数据
	 *
	 * @param columns
	 * @param orderBy
	 * @param count
	 * @return
	 */
	List<M> findListByColumns(Columns columns, String orderBy, Integer count);

	/**
	 * 根据多个 id 查找多个对象
	 *
	 * @param ids
	 * @return
	 */
	List<M> findListByIds(Object... ids);

	/**
	 * 根据提交查询数据量
	 *
	 * @param columns
	 * @return
	 */
	long findCountByColumns(Columns columns);

	/**
	 * 根据ID 删除model
	 *
	 * @param id
	 * @return
	 */
	boolean deleteById(Object id);

	/**
	 * 删除
	 *
	 * @param model
	 * @return
	 */
	boolean delete(M model);

	/**
	 * 根据 多个 id 批量删除
	 *
	 * @param ids
	 * @return
	 */
	boolean batchDeleteByIds(Object... ids);

	/**
	 * 保存到数据库
	 *
	 * @param model
	 * @return id if success
	 */
	Object save(M model);

	/**
	 * 保存或更新
	 *
	 * @param model
	 * @return id if success
	 */
	Object saveOrUpdate(M model);

	/**
	 * 更新
	 *
	 * @param model
	 * @return
	 */
	boolean update(M model);

	/**
	 * 分页
	 *
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Page<M> paginate(int page, int pageSize);

	/**
	 * 分页
	 *
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Page<M> paginateByColumns(int page, int pageSize, Columns columns);

	/**
	 * 分页
	 *
	 * @param page
	 * @param pageSize
	 * @param columns
	 * @param orderBy
	 * @return
	 */
	Page<M> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

	/**
	 * 用于给子类复写，用于刷新缓存
	 *
	 * @param action
	 * @param model
	 * @param id
	 */
	void shouldUpdateCache(int action, Model model, Object id);

}